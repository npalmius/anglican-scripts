#!/bin/bash

dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

export lines=$1
export cmd=$2

awk -f "$dir/awk_scripts/buffer_output.awk" -
