BEGIN{ FS=","; }
{
  gsub(/[)(]/, "", $0);
  print $2;
  fflush();
}
