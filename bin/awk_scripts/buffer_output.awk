function print_output()
{
    close(f);
    system("export sweep=" j " && cat " f " | " command_to_run);
    system("[ -f " f " ] && rm " f);
    fflush();
}
BEGIN{
  i = 0;
  j = 1;
  mktmp = "mktemp";
  mktmp | getline f;
  lines_to_buffer = ENVIRON["lines"]
  command_to_run = ENVIRON["cmd"]
}
{
  if (i == lines_to_buffer) {
    print_output();
    i = 1;
	j = j + 1;
  } else {
    i = i + 1;
  }
  print $0 > f;
}
END{
  print_output();
}