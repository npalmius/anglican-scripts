#!/bin/bash

dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

cat - | tr -s " " | cut -d " " -f "$@"
