#Anglican Support Scripts

This repository contains a set of support scripts to use with the 
[Anglican](http://www.robots.ox.ac.uk/~fwood/anglican/) 
probabilistic programming language.
