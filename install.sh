#!/bin/bash

dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

echo "export PATH=$dir/bin:\$PATH" > $dir/bin/register_path

echo "To install permanently, add the following line to your ~/.profile or ~/.bashrc files as required"
echo ""
echo " source $dir/bin/register_path"
echo ""
echo "To take effect immediately, run the above command now."
